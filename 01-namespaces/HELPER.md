# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

### View namespaces
```bash
kubectl get namespaces
```

### Set namespace config in `kubectl`
```bash
## set namespace context
kubectl config set-context $(kubectl config current-context) --namespace=#########
```

### Get details on namespace
```bash
## Get details on namespace
kubectl describe namespace
```

### Namespace YAML
```yaml
apiVersion: v1
kind: Namespace
metadata:
  label:
    environment: dev
  name: <YOUR NAMESPACE NAME HERE>
```

### View All Pods
Run the `kubectl get pods` in all namespaces

```bash
⎋ : kubectl get po --all-namespaces
NAMESPACE     NAME                                                    READY     STATUS    RESTARTS   AGE
kube-system   event-exporter-v0.1.9-85bb4fd64d-hs7r5                  2/2       Running   0          46m
kube-system   fluentd-gcp-scaler-7c5db745fc-klxml                     1/1       Running   0          46m
kube-system   fluentd-gcp-v3.0.0-9rnfc                                2/2       Running   0          39m
kube-system   fluentd-gcp-v3.0.0-cj859                                2/2       Running   0          37m
kube-system   fluentd-gcp-v3.0.0-x6c2p                                2/2       Running   0          40m
kube-system   heapster-v1.5.2-6c7d586bdd-p8vth                        3/3       Running   0          45m
kube-system   kube-dns-788979dc8f-v6s48                               4/4       Running   0          45m
kube-system   kube-dns-788979dc8f-x8dlg                               4/4       Running   0          46m
kube-system   kube-dns-autoscaler-79b4b844b9-rrk58                    1/1       Running   0          46m
kube-system   kube-proxy-gke-prod-master-default-pool-68dc877b-f54f   1/1       Running   0          46m
kube-system   kube-proxy-gke-prod-master-default-pool-68dc877b-pgnt   1/1       Running   0          46m
kube-system   kube-proxy-gke-prod-master-prod-node-12f1fdc7-w9zj      1/1       Running   0          45m
kube-system   kubernetes-dashboard-598d75cb96-lv42j                   1/1       Running   0          46m
kube-system   l7-default-backend-5d5b9874d5-tps9x                     1/1       Running   0          46m
kube-system   metrics-server-v0.2.1-7486f5bd67-crhpr                  2/2       Running   0          45m
```

# Limiting Resources

## Set HARD limits on ALL resources in a namespace

```yaml
#...
#...
spec:
  hard:
    requests.cpu: "1"
    requests.memory: 1Gi
    limits.cpu: "2"
    limits.memory: 2Gi
#...
#...
```

