# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

## Notes

* ConfigMaps are only read on creation of Pods, rolling update can re-apply changes to ConfigMaps
* Create ConfigMap before deployments

## Commands

### Forward for Debug

Local development can access debugger on localhost:5005

```bash
kubectl port-forward <POD>  5005:5005

# Our service
kubectl port-forward deployment/simple-echo-service-deployment 8080:8080
```

### Create & Update ConfigMap
```bash
kubectl apply -f configuration.yaml
```

### Create ConfigMap from Properties file
```bash
# Example
kubectl create configmap system-properteis --from-file=05-configuration/system.properties
```
