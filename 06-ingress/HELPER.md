# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

## Commands

### Ingress Creation
```bash
# Get the service
kubectl get svc simple-echo-service-np

# If not a failure

kubectl apply -f simple-echo-service-np-ingress.yaml
```