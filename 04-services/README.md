
# Services

## Goals

* Understand three methods to expose Deployments
* Learn how to debug Services
* Manage lifecycle of a Service

## Done
* Created & connected to a ClusterIP based service
* Craated a LoadBalancer service and visited the IP
* No services in namespace when completed

## Navigation
[Deployments](../03-deployments) | [Configuration](../05-configuration "05-configuration lab") | [I need help!!](HELPER.md)

---

## Exercises

1. Deploy an `nginx` pod with a **ClusterIP** based endpoint
1. Create `kubectl proxy --port=8081` and navigate browser to service
    * http://localhost:8081/api/v1/namespaces/<YOURNAMESPCE>/services/<SERVICE_NAME>:<PORT_NAME>/proxy/
1. Create a new service using a **NodePort** endpoint
    * http://localhost:8081/api/v1/namespaces/<YOURNAMESPCE>/services/<SERVICE_NAME>:<PORT_NAME>/proxy/
    * **How many Pods are running?**
    * **What can go wrong?  What about removing the other service's _deployment_?**
1. Update deployment to use separate Pods
1. Create a new deployment using LoadBalancer
1. Navigate to IP+Port on browser
    * Validate by browsing to EXTERNAL_IP in a browser
1. Clean-up environment by removing all services, deployments and pods


## NOTES

+ Serivces expose Pods by label/selector, Deployments make the number of Pods available and attribute with labels
+ DNS Resolution `<service-name>.<namespace-name>.svc.cluster.local`
