echo -n "Enter the name of the state bucket [ENTER]: "
read state_bucket_name
#echo -n "Enter your gender and press [ENTER]: "
#read -n 1 gender
echo

echo $state_bucket_name
echo 
#echo $gender


# Validate Environment Variables
#if [ -z "$TF_ADMIN" ]; then echo "TF_ADMIN is not set";  exit 1; fi
if [ -z "$TF_VAR_billing_account" ]; then echo "TF_VAR_billing_account is not set";  exit 1; fi
if [ -z "$TF_VAR_org_id" ]; then echo "TF_VAR_org_id is not set";  exit 1; fi


## Ask if Terrform Service Account is available
random_string=$(shuf -i 2000-65000 -n 1)

echo $random_string

## Build state backend
echo "gsutil mb -p ${TF_ADMIN} gs://${state_bucket_name}-${random_string}"
  ## Check with gsutils, does bucket exist?
  ## Create bucket (gsutils)
  ## Print bucket name
  ## limit access to ONLY TF SA
## 

## TF Foundation
  ## terraform plan && terraform apply -auto-approve

## TF Services
  ## terraform plan && terraform apply -auto-approve
