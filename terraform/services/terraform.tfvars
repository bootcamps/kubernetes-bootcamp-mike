# Cluster Name
name = "primary"

# Carried over from "foundation" run
project = "kubernetes-bootcamp-project"

env = "prod"

region = "us-west1"

zone = "us-west1-a"

cluster_count = 1 ## TODO

machine_type = "n1-standard-1"

tags = ["k8-node"]

username = "admin" # The username to use for HTTP basic authentication when accessing the Kubernetes master endpoint

# Needs at least 16
password = "skookumchuck123456"

kubernetes_dashboard = true

horizontal_pod_autoscaling_disable = false

maximumNodeCount = 5
