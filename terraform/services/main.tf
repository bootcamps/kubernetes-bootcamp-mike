provider "google" {
  # This is the SA for Terraform
  credentials = "${file("/Users/mensor/.config/gcloud/mensor-tf-admin.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
  version     = "~> 1.8"
}

provider "random" {
  version = "~> 1.1"
}

locals {
  credentials_file_path = "$var.credentials"
}

provider "google" {
  credentials = "${file(local.credentials_file_path)}"
}

module "gke" {
  source            = "github.com/terraform-google-modules/terraform-google-kubernetes-engine?ref=v0.2.0"
  project_id        = "${var.project_id}"
  name              = "kubernetes-workshop-cluster"
  region            = "${var.region}"
  network           = "${var.network}"
  subnetwork        = "${var.subnetwork}"
  ip_range_pods     = "${var.ip_range_pods}"
  ip_range_services = "${var.ip_range_services}"
}

# module "gke" {
#   # NOTE, double // for module path
#   source = "git::https://gitlab.com/dev9-labs/terraform-gcp-kubernetes//modules/gke"

#   env  = "${var.env}"
#   zone = "${var.zone}"
#   name = "${var.name}"

#   username = "${var.username}"
#   password = "${var.password}"
#   project  = "${var.project}"

#   maxNodeCount_node = "${var.maximumNodeCount}"
#   cluster_count     = 2

#   # 1x = $48/month, 3x = $145/month
#   machine_type_node = "n1-standard-2"

#   horizontal_pod_autoscaling_disable = false
# }
